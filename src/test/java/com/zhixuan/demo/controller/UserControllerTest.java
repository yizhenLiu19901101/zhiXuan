package com.zhixuan.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.zhixuan.demo.utils.LoggerUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * userController测试类
 * @WebAppConfuguration注解用来表示测试环境使用的ApplicationContext将是WebApplicationContext类型的
 * @RunWith指定一个运行器
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserControllerTest {

    /**
     * 注入网络应用环境
     */
    @Autowired
    private WebApplicationContext wac;
    /**
     * Mock
     */
    private MockMvc mockMvc;

    /**
     * 测试之前需要完成的动作
     */
    @Before
    public void setup() {
        //构造MockMvc
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).alwaysDo(MockMvcResultHandlers.print())
                                      .build();
    }


    /**
     * 新增用户信息
     * @throws Exception
     */
    @Test
    public void addUser() throws Exception {
        //设置查询条件
        JSONObject userCondition = new JSONObject();
        userCondition.put("userName","xuexue");
        userCondition.put("password","19910307");
        userCondition.put("isDelete",0);
        userCondition.put("reversion",1);
        LoggerUtils.info(getClass(),"查询条件为: " + userCondition.toJSONString());
        //执行测试
        mockMvc.perform(MockMvcRequestBuilders.put("/api/user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(userCondition.toJSONString().getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * 更新用户信息
     * @throws Exception
     */
    @Test
    public void updateUser() throws Exception {
        //设置查询条件
        JSONObject userCondition = new JSONObject();
        userCondition.put("id","1");
        userCondition.put("userName","aiolos");
        userCondition.put("reversion",2);
        //执行测试
        mockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(userCondition.toJSONString().getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * 查询用户信息列表
     * @throws Exception
     */
    @Test
    public void queryUsers() throws Exception {
        LoggerUtils.info(getClass(),"查询所有用户的信息");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }



    /**
     * 查询满足条件的用户信息列表
     * @throws Exception
     */
    @Test
    public void queryUsersByCondition() throws Exception {
        LoggerUtils.info(getClass(),"根据条件查询用户信息");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user").param("nickName","xuexue"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * 删除用户信息
     * @throws Exception
     */
    @Test
    public void deleteUser() throws Exception {
        //设置查询条件
        JSONObject userCondition = new JSONObject();
        userCondition.put("id","1");
        userCondition.put("reversion",4);
        //执行测试
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(userCondition.toJSONString().getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
