package com.zhixuan.demo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志工具类
 * @author  milo
 */
public class LoggerUtils {

    /**
     * 打印日志信息
     * @param className，可以指向任何类型
     * @param message
     */
    public static void info(Class<?> className,String message){
        Logger logger = LoggerFactory.getLogger(className);
        if(logger.isInfoEnabled()){
            logger.info(message);
        }

    }
}
