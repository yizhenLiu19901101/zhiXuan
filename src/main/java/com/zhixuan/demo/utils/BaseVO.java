package com.zhixuan.demo.utils;

import com.google.gson.annotations.Expose;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 基本输出格式
 * @author  milo
 * @date 2018-09-26
 */
@Data
public class BaseVO {
    /**
     * 返回码
     */
    @Expose
    @ApiModelProperty(name = "code",value = "返回码")
    private int code;
    /**
     * 返回信息
     */
    @Expose
    @ApiModelProperty(name = "msg",value = "返回消息")
    private String msg;

    /**
     * 含参构造器
     * @param code
     * @param msg
     */
    public BaseVO(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 无参构造器
     */
    public BaseVO() {
    }
}
