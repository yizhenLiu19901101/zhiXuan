package com.zhixuan.demo.utils;

import com.google.gson.annotations.Expose;
import com.zhixuan.demo.enumeration.ErrorListEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 通用数据传输对象
 * @author milo
 * @date 2018-09-26
 * @param <T>
 */
@ApiModel(value="返回对象",description="返回对象generalVO")
public class GeneralVO<T> extends BaseVO {
    @Expose
    @ApiModelProperty(name = "body",value = "返回体")
    private T body;

    public GeneralVO() {
    }

    public GeneralVO(int code, String msg, T body) {
        this.setCode(code);
        this.setMsg(msg);
        this.setBody(body);
    }

    public GeneralVO(ErrorListEnum listEnum, T body) {
        this.setCode(listEnum.getKey());
        this.setMsg(listEnum.getValue());
        this.setBody(body);
    }

    public T getBody() {
        return this.body;
    }

    public void setBody(T t) {
        this.body = t;
    }

    @Override
    public String toString() {
        String result = '{' + "\"code\":" + this.getCode() + ',' + "\"msg\":\"" + this.getMsg() + "\"," + "\"body\":" + this.getBody() + '}';
        return result;
    }
}
