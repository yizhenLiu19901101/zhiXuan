package com.zhixuan.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhixuan.demo.entity.dto.UserDTO;
import com.zhixuan.demo.entity.vo.UserVO;
import org.apache.ibatis.annotations.CacheNamespace;
import java.util.List;

/**
 * 用户dao
 * @author  milo
 * @version  1.0.0
 */
@CacheNamespace
public interface UserMapper extends BaseMapper<UserVO> {
    /**
     * 分页查询用户列表
     * @param userDTO
     * @return
     */
    List<UserVO> selectUserListByPage(UserDTO userDTO);

}
