package com.zhixuan.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhixuan.demo.entity.vo.FinanceVO;

/**
 *  * 财务dao
 *  * @author  milo
 *  * @version  1.0.0
 */
public interface FinanceMapper extends BaseMapper<FinanceVO> {
}
