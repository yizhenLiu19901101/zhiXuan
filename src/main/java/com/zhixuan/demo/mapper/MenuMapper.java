package com.zhixuan.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhixuan.demo.entity.vo.MenuVO;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 用户dao
 * @author  milo
 * @version  1.0.0
 */
@CacheNamespace
public interface MenuMapper extends BaseMapper<MenuVO> {
}
