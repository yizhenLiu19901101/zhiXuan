package com.zhixuan.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhixuan.demo.entity.vo.MenuVO;
import com.zhixuan.demo.enumeration.ErrorListEnum;
import com.zhixuan.demo.enumeration.FlagListEnum;
import com.zhixuan.demo.mapper.MenuMapper;
import com.zhixuan.demo.utils.GeneralVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * 菜单操作控制器类
 * @author  milo
 * @date 2019-02-12
 */
@Api(value="菜单操作controller",tags={"菜单操作接口"})
@RestController
@EnableAutoConfiguration
public class MenuController {

    @Autowired
    private MenuMapper menuMapper;

    /**
     * 获取所有的菜单列表
     * @return
     */
    @ApiOperation(value="获取所有菜单信息", notes="获取所有的菜单信息")
    @GetMapping("/api/menus")
    public GeneralVO queryAllMenus() {
        //设置查询条件
        QueryWrapper<MenuVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", FlagListEnum.FLG_LIST_NOT_DELETE.getKey());
        //查询
        List<MenuVO> menuList = menuMapper.selectList(queryWrapper);
        return new GeneralVO(ErrorListEnum.ERROR_LIST_SUCCESS,menuList);
    }


}
