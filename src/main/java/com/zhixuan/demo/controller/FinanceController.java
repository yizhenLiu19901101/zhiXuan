package com.zhixuan.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhixuan.demo.entity.dto.FinanceDTO;
import com.zhixuan.demo.entity.vo.FinanceVO;
import com.zhixuan.demo.entity.vo.MenuVO;
import com.zhixuan.demo.enumeration.ErrorListEnum;
import com.zhixuan.demo.enumeration.FlagListEnum;
import com.zhixuan.demo.mapper.FinanceMapper;
import com.zhixuan.demo.mapper.MenuMapper;
import com.zhixuan.demo.utils.GeneralVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 菜单操作控制器类
 * @author  milo
 * @date 2019-02-12
 */
@Api(value="财务操作controller",tags={"财务操作接口"})
@RestController
@EnableAutoConfiguration
public class FinanceController {
    @Autowired
    private FinanceMapper financeMapper;

    /**
     * 获取所有的菜单列表
     * @return
     */
    @ApiOperation(value="查询财务信息", notes="查询财务信息")
    @GetMapping("/api/finances")
    public GeneralVO queryFinancesByCondition() {
        //设置查询条件
        QueryWrapper<FinanceVO> queryWrapper = new QueryWrapper<>();
        //查询
        List<FinanceVO> financeList = financeMapper.selectList(queryWrapper);
        return new GeneralVO(ErrorListEnum.ERROR_LIST_SUCCESS,financeList);
    }

}
