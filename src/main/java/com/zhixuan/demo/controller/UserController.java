package com.zhixuan.demo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhixuan.demo.entity.vo.UserVO;
import com.zhixuan.demo.enumeration.ErrorListEnum;
import com.zhixuan.demo.enumeration.FlagListEnum;
import com.zhixuan.demo.mapper.UserMapper;
import com.zhixuan.demo.utils.GeneralVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;

/**
 * 用户操作控制器类
 * @author  milo
 * @date 2018-09-26
 */
@Api(value="用户操作controller",tags={"用户操作接口"})
@RestController
@EnableAutoConfiguration
public class UserController {

    @Autowired
    private UserMapper userMapper;

    /**
     * 查询满足条件的用户信息列表
     * @return
     */
    @ApiOperation(value="获取所有用户的信息", notes="获取所有用户的信息")
    @GetMapping("/api/users")
    public GeneralVO queryUsers() {
        //设置查询条件
        QueryWrapper<UserVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", FlagListEnum.FLG_LIST_NOT_DELETE.getKey());
        //查询
        List<UserVO> userList = userMapper.selectList(queryWrapper);
        return new GeneralVO(ErrorListEnum.ERROR_LIST_SUCCESS,userList);
    }

    /**
     * 查询满足条件的用户信息列表
     * @param nickName
     * @return
     */
    @ApiOperation(value="根据条件查询用户的信息", notes="根据条件查询用户的信息")
    @GetMapping("/api/user")
    public GeneralVO queryUsersByCondition(@ApiParam(name="nickName",value="昵称",required=true) @RequestParam("nickName") String nickName) {
        //设置查询条件
        QueryWrapper<UserVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", FlagListEnum.FLG_LIST_NOT_DELETE.getKey());
        queryWrapper.like("user_name",nickName);
        //查询
        List<UserVO> userList = userMapper.selectList(queryWrapper);
        return new GeneralVO(ErrorListEnum.ERROR_LIST_SUCCESS,userList);
    }

    /**
     * 新增用户信息
     * @param userVO
     * @return
     */
    @PutMapping("/api/user")
    @ApiOperation(value="新增用户信息", notes="新增用户信息")
    public GeneralVO inertUser(@RequestBody UserVO userVO) {
        //生成数据库主键
        userVO.setCreatedTime(new Date());
        userVO.setUpdatedTime(new Date());
        userVO.setIsDelete(FlagListEnum.FLG_LIST_NOT_DELETE.getKey());
        int userId = userMapper.selectCount(null) + 1;
        userVO.setUserId(userId);
        userVO.setReversion(1);
        userVO.setUpdatedBy("123456");
        userVO.setCreatedBy("123456");
        Integer result = userMapper.insert(userVO);
        if(result != null && result == 1){
            return new GeneralVO(ErrorListEnum.ERROR_LIST_SUCCESS,null);
        }
        else{
            return new GeneralVO(ErrorListEnum.ERROR_LIST_FAIL,null);
        }
    }

    /**
     * 修改用户信息
     * @param userVO
     * @return
     */
    @ApiOperation(value="修改用户的个人信息", notes="修改用户的个人信息")
    @PostMapping("/api/user")
    public GeneralVO updateUser(@RequestBody UserVO userVO) {
        userVO.setUpdatedTime(new Date());
        userVO.setUpdatedBy("123456");
        Integer result = userMapper.updateById(userVO);
        if(result != null && result == 1){
            return new GeneralVO(ErrorListEnum.ERROR_LIST_SUCCESS,null);
        }else{
            return new GeneralVO(ErrorListEnum.ERROR_LIST_FAIL,null);
        }
    }

    /**
     * 删除用户信息
     * @param userVOCondition
     * @return
     */
    @ApiOperation(value="删除用户的个人信息", notes="删除用户的个人信息")
    @DeleteMapping("/api/user")
    public GeneralVO deleteUser(@RequestBody UserVO userVOCondition) {
        UserVO userVO = UserVO.builder().id(userVOCondition.getId()).reversion(userVOCondition.getReversion()).isDelete(FlagListEnum.FLG_LIST_DELETED.getKey()).updatedTime(new Date())
                .createdBy("123456").build();
        //按照ID删除用户信息
        Boolean result = userVO.updateById();
        if(result != null && result){
            return new GeneralVO(ErrorListEnum.ERROR_LIST_SUCCESS,null);
        }
        else{
            return new GeneralVO(ErrorListEnum.ERROR_LIST_FAIL,null);
        }
    }
}


