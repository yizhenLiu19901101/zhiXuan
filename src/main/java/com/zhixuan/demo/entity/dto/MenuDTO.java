package com.zhixuan.demo.entity.dto;

/**
 * 菜单数据传输对象
 * @author  milo
 * @date 2018-09-26
 */
public class MenuDTO {
    /**
     * 菜单层级
     */
    private Integer menuLevel;

    /**
     * 用户ID
     */
    private String userId;
}
