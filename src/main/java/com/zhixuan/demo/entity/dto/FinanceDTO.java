package com.zhixuan.demo.entity.dto;


/**
 * 用户财务数据数据传书对象
 */
public class FinanceDTO{
    /**
     * 用户ID
     */
    private String userId;

    /**
     * 查询类型(1代表近一周，2代表近一月，3代表近一季度，4代表近一年)
     */
    private String queryType;
}
