package com.zhixuan.demo.entity.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;


@Data
@Builder
@TableName("user_finances")
public class FinanceVO extends Model<FinanceVO> implements Serializable {

    /**
     * 数据库主键
     */
    @TableId(value = "id",type = IdType.UUID)
    private String id;

    /**
     * 金额
     */
    @TableField(value = "amount")
    private BigDecimal amount = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

    /**
     * 更新时间
     */
    @TableField(value="updated_time")
    private Date updatedTime;

    /**
     * 获得主键值
     * @return
     */
    @Override
    protected Serializable pkVal() {
        return this.getId();
    }
}
