package com.zhixuan.demo.entity.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户实体类
 * @author  milo
 * @date 2018-09-26
 */
@Data
@Builder
@TableName("users")
public class UserVO extends Model<UserVO> implements Serializable {

    /**
     * 数据库主键
     */
    @TableId(value = "id",type = IdType.UUID)
    private String id;

    /**
     * 业务主键
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;
    /**
     * 密码
     */
    @TableField("password")
    private String password;
    /**
     * 版本
     */
    @TableField("reversion")
     private Integer reversion;
    /**
     * 创建时间
     */
    @TableField("created_time")
    private Date createdTime;

    /**
     * 创建人
     */
    @TableField("created_by")
    private String createdBy;

    /**
     * 修改时间
     */
    @TableField("updated_time")
    private Date updatedTime;

    /**
     * 修改人
     */
    @TableField("updated_by")
    private String updatedBy;

    /**
     * 删除标志，0代表未删除，1代表已删除
     */
    @TableField("is_delete")
    private Integer isDelete;

    /**
     * 获得主键值
     * @return
     */
    @Override
    protected Serializable pkVal() {
        return this.getId();
    }
}
