package com.zhixuan.demo.entity.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@TableName("menus")
public class MenuVO extends Model<MenuVO> implements Serializable {
    /**
     * 数据库主键
     */
    @TableId(value = "id",type = IdType.UUID)
    private String id;
    /**
     * 菜单ID
     */
    @TableField(value = "menu_id")
    private String menuId;

    /**
     * 菜单名称
     */
    @TableField(value = "menu_name")
    private String menuName;


    /**
     * 父菜单ID
     */
    @TableField(value = "parent_menu_id")
    private String parentMenuId;

    /**
     * 菜单层级
     */
    @TableField(value="menu_level")
    private Integer menuLevel;

    /**
     * 获得主键值
     * @return
     */
    @Override
    protected Serializable pkVal() {
        return this.getId();
    }
}
