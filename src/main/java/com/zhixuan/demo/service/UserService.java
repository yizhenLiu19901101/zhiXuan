package com.zhixuan.demo.service;


import com.zhixuan.demo.entity.dto.UserDTO;
import com.zhixuan.demo.entity.vo.UserVO;
import com.zhixuan.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * 用户业务类
 * @author  milo
 */
@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 分页查找用户信息
     * @param userDto
     * @return
     */
     public List<UserVO> selectPage(UserDTO userDto) {
         return  userMapper.selectUserListByPage(userDto);
     }

}
