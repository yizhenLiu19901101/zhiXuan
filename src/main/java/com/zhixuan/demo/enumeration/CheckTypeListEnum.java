package com.zhixuan.demo.enumeration;

/**
 * 查询类型
 * @author  milo
 * @date 2019-02-12
 */
public enum CheckTypeListEnum {

    THIS_WEEK(1,"近一周");
    private final int key;
    private final String value;

    private CheckTypeListEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }

    public static String getValue(int key) {
        CheckTypeListEnum[] var1 = values();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            CheckTypeListEnum e = var1[var3];
            if (e.getKey() == key) {
                return e.value;
            }
        }
        return null;
    }
}
